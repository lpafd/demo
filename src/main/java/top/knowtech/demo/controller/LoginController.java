package top.knowtech.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @Autowired
    JdbcTemplate jdbc;


    @RequestMapping("/login")
    public String login(String uname, String upass) {
        int i = jdbc.queryForObject("select count(*) from users where uname=? and upass=?", Integer.class, new Object[]{uname, upass});
        if(i == 1) {
            return "default";
        } else {
            return "fail";
        }
    }

    @RequestMapping("/adduser")
    public String adduser() {
        return "adduser";
    }


    @RequestMapping("/add") 
    public String add(String uname,String upass, String reupass) {
        if(!upass.equals(reupass)) {
            return "fail";
        }
        if(uname.length() < 2) {
            return "fail";
        }
        int i  = jdbc.update("insert into users values(null ,?,?) ", new Object[] {uname,upass});

        if (i >0) {
            return "success";
        } else {
            return "fail";
        }
    }

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping("/success")
    public String success() {
        return "success";
    }   

}
